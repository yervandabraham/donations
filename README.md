# Magento 2 Module AYKO Donations

    ayko/module-donations

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Donations module

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/AYKO`
 - Enable the module by running `php bin/magento module:enable AYKO_Donations`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require ayko/module-donations`
 - enable the module by running `php bin/magento module:enable AYKO_Donations`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Enabled (donations/general/active)
 - Donation Amounts (donations/general/donation_amounts)


## Specifications

 - Module AYKO_Donations
 - Ability to add a donation at the checkout after the item view section
 - Config option in the admin that adds ability to confirm amounts that are to be added to the donation form (For example, I could add 10, 50, and 100 into the admin and then they would display in the checkout)
 - Add the value donated to the order
 - Display new total for the donation throughout all views of orders / invoices / shipments etc
 - "My Donations" link in the customer account to display all previous donations by that customer and a total for all donations added together
 - Donations report in the admin report section for any orders added with donations
 - Donation amount as a column on the order grid
 - Column on the customer grid for lifetime donations
 - Donation section on the admin customer view for previous donations and lifetime value of donations



## Attributes

 - Sales - Donation (donation)

