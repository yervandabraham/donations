<?php
namespace AYKO\Donations\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddDonationInvoiced implements ObserverInterface
{
    /**
     * Set donation invoiced to order
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $invoice = $observer->getInvoice();
        $order = $observer->getOrder();

        $order->setDonationInvoiced($order->getDonationInvoiced() + $invoice->getDonationAmount());
        $order->setBaseDonationInvoiced($order->getBaseDonationInvoiced() + $invoice->getBaseDonationAmount());

		return $this;
    }
}
