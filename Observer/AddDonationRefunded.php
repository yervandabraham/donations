<?php
namespace AYKO\Donations\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddDonationRefunded implements ObserverInterface
{
    /**
     * Set donation refunded to order
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $creditmemo = $observer->getCreditmemo();
        $order = $creditmemo->getOrder();

        $order->setDonationRefunded($order->getDonationRefunded() + $creditmemo->getDonationAmount());
        $order->setBaseDonationRefunded($order->getBaseDonationRefunded() + $creditmemo->getBaseDonationAmount());

		return $this;
    }
}
