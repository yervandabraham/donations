<?php
namespace AYKO\Donations\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddDonationToOrder implements ObserverInterface
{
    /**
     * Set donation to order
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $quote = $observer->getQuote();
        $donation = $quote->getDonationAmount();
        if (!$donation) {
            return $this;
        }
        $baseDonation = $quote->getBaseDonationAmount();

        //Set donation amount to order
        $order = $observer->getOrder();
        $order->setData('donation_amount', $donation)
            ->setData('base_donation_amount', $baseDonation);

		return $this;
    }
}
