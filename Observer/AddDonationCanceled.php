<?php
namespace AYKO\Donations\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddDonationCanceled implements ObserverInterface
{
    /**
     * Set donation canceled to order
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $order = $observer->getOrder();

        $order->setDonationCanceled($order->getDonationAmount() - $order->getDonationInvoiced());
        $order->setBaseDonationCanceled($order->getBaseDonationAmount() - $order->getBaseDonationInvoiced());

		return $this;
    }
}
