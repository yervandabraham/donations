<?php
namespace AYKO\Donations\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use \Magento\Checkout\Model\Session;
use AYKO\Donations\Helper\Data;

class AddPaymentDonationItem implements ObserverInterface
{
    /**
     * @var Session
     */
    protected $checkout;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * AddPaymentDonationItem constructor.
     *
     * @param Session $checkout
     * @param Data $helper
     */
    public function __construct(Session $checkout,
                                Data $helper
    )
    {
        $this->checkout = $checkout;
        $this->helper = $helper;
    }

    /**
     * Add Payment Donation Item
     *
     * @param Observer $observer
     * @return $this|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        if(!$this->helper->isModuleEnabled()){
            return $this;
        }
        $cart = $observer->getEvent()->getCart();
        $quote = $this->checkout->getQuote();
        $donationAmount = $quote->getDonationAmount();
        if($donationAmount) {
            $cart->addCustomItem(__('Donation'), 1, $donationAmount, 'donation');
        }
    }
}