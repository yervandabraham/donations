<?php
namespace AYKO\Donations\Model\Order\Creditmemo\Total;

use Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal;

class Donation extends AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo)
    {
        $creditmemo->setDonationAmount(0);
        $creditmemo->setBaseDonationAmount(0);

        $order = $creditmemo->getOrder();

        $creditmemo->setDonationAmount($order->getDonationAmount());
        $creditmemo->setBaseDonationAmount($order->getBaseDonationAmount());

        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getDonationAmount());
        $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $creditmemo->getBaseDonationAmount());

        return $this;
    }
}
