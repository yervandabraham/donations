<?php
namespace AYKO\Donations\Model\Order\Invoice\Total;

use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class Donation extends AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $invoice->setDonationAmount(0);
        $invoice->setBaseDonationAmount(0);

        $order = $invoice->getOrder();

        $invoice->setDonationAmount($order->getDonationAmount());
        $invoice->setBaseDonationAmount($order->getBaseDonationAmount());

        $invoice->setGrandTotal($invoice->getGrandTotal() + $invoice->getDonationAmount());
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $invoice->getBaseDonationAmount());

        return $this;
    }
}
