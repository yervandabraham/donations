<?php
namespace AYKO\Donations\Model\Quote\Address\Total;

/**
 * Donation totals calculation model.
 */
class Donation extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    protected $_code = 'donation';

    /**
     * Collect total donation
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        $items = $shippingAssignment->getItems();
        if (!count($items)) {
            return $this;
        }
        $total->setTotalAmount($this->getCode(), 0);
        $total->setBaseTotalAmount($this->getCode(), 0);

        $donation = $quote->getDonationAmount();
        $baseDonation = $quote->getBaseDonationAmount();

        $total->setTotalAmount($this->getCode(), $donation);
        $total->setBaseTotalAmount($this->getCode(), $baseDonation);
        $total->setDonation($donation);
        $total->setBaseDonation($baseDonation);
        $total->setGrandTotal($total->getGrandTotal() + $donation);
        $total->setBaseGrandTotal($total->getBaseGrandTotal() + $baseDonation);

        return $this;
    }

    /**
     * Add donation total information to address
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return array|null
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $result = null;
        $amount = $quote->getDonationAmount();

        if ($amount != 0) {
            $result = [
                'code' => $this->getCode(),
                'title' => __('Donation'),
                'value' => $amount
            ];
        }
        return $result;
    }
}
