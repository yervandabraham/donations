<?php
namespace AYKO\Donations\Model\ResourceModel\Report\Order\Updatedat;

/**
 * Report order updated_at collection
 */
class Collection extends \AYKO\Donations\Model\ResourceModel\Report\Order\Collection
{
    /**
     * Aggregated Data Table
     *
     * @var string
     */
    protected $_aggregationTable = 'sales_donation_aggregated_updated';
}
