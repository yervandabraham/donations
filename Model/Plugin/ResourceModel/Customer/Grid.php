<?php
namespace AYKO\Donations\Model\Plugin\ResourceModel\Customer;
 
class Grid
{
    public static $table = 'customer_grid_flat';
    public static $orderTable = 'sales_order';
 
    public function afterSearch($intercepter, $collection)
    {
        if ($collection->getMainTable() === $collection->getConnection()->getTableName(self::$table)) {
            $orderTableName = $collection->getConnection()->getTableName(self::$orderTable);
            $collection->getSelect()
                ->joinLeft(
                    ['o'=>$orderTableName],
                    "o.customer_id = main_table.entity_id",
                    [
                        'donation_amount' => 'SUM(o.base_donation_amount)'
                    ]
                );
            $where = $collection->getSelect()->getPart(\Magento\Framework\DB\Select::WHERE);
            $collection->getSelect()->setPart(\Magento\Framework\DB\Select::WHERE, $where)->group('main_table.entity_id');
        }
        return $collection;
    }
}