define([
    'ko',
    'jquery',
    'uiComponent',
    'mage/storage',
    'mage/url',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/totals',
    'Magento_Checkout/js/model/cart/totals-processor/default'
], function (ko, $, Component, storage, url, quote, totals, totalsProcessor) {
    'use strict';

    return Component.extend({
        isActive: ko.observable(0),
        donations: ko.observable([]),
        donationAmount: ko.observable(0),
        initialize: function () {
            this._super();
            this.getDonationValues();

            return this;
        },
        getDonationValues: function () {
            var serviceUrl = url.build('donations/checkout/donation');
            var self = this;
            storage.get(serviceUrl).done(
                function (response) {
                    if (response.success) {
                        self.donations(response.donations);
                        self.donationAmount(response.amount);
                        self.isActive(response.active);
                        return true;
                    }
                }
            ).fail(
                function (response) {
                    return response.value
                }
            );
            return false;
        },
        onDonate: function (amount) {
            console.log(amount);
            var serviceUrl = url.build('donations/checkout/donation');
            var self = this;
            $.ajax({
                url: serviceUrl,
                data: {'amount': amount},
                type: 'post',
                dataType: 'json'
            })
                .done(function (response) {
                    if (response.success) {
                        self.donations(response.donations);
                        self.donationAmount(response.amount);
                        self.isActive(response.active);
                        totals.isLoading(true);
                        totalsProcessor.estimateTotals(quote.shippingAddress()).done(function () {
                            totals.isLoading(false);
                        });

                        return true;
                    }
                })
                .fail(
                    function (response) {
                        return response.value
                    }
                );
            return false;
        },
    });
});