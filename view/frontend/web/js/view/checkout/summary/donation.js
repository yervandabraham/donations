define([
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/totals'
], function (Component, quote, totals) {
    'use strict';

    return Component.extend({
        totals: quote.getTotals(),

        isDisplayed: function() {
            return this.getPureValue() != 0;
        },

        /**
         * Get pure value.
         *
         * @return {*}
         */
        getPureValue: function () {
            var value = 0;
            if (this.totals() && totals.getSegment('donation')) {
                value = totals.getSegment('donation').value;
            }
            console.log(value);
            return value;
        },

        /**
         * @return {*|String}
         */
        getValue: function () {
            return this.getFormattedPrice(this.getPureValue());
        }

    });
});