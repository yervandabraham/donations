<?php
namespace AYKO\Donations\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * Donations config path
     */
    const CONFIG_DONATION_IS_ENABLED = 'donations/general/active';
    const CONFIG_DONATION_AMOUNTS = 'donations/general/donation_amounts';

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $_serializer;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Serialize\SerializerInterface $serializer
    ) {
        $this->_serializer = $serializer;
        parent::__construct($context);
    }

    /**
     * Is donations module enabled
     *
     * @return mixed
     */
    public function isModuleEnabled()
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_DONATION_IS_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get serialized donation amounts
     *
     * @return mixed
     */
    public function getDonationAmountsSerialized()
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_DONATION_AMOUNTS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get donation amounts
     *
     * @return mixed
     */
    public function getDonationAmounts()
    {
        return $this->_serializer->unserialize($this->getDonationAmountsSerialized());
    }

    /**
     * Is donation amount allowed
     *
     * @param $amount
     * @return bool
     */
    public function isDonationAllowed($amount)
    {
        $allowDonation = ($amount == 0);
        if(!$allowDonation) {
            foreach ($this->getDonationAmounts() as $donationAmount) {
                if ($donationAmount['amount'] == $amount) {
                    $allowDonation = true;
                    break;
                }
            }
        }
        return $allowDonation;
    }

    /**
     * Log info message
     *
     * @param $message
     */
    public function logInfo($message)
    {
        $this->_logger->info($message);
    }
}
