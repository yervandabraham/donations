<?php
namespace AYKO\Donations\Controller\Checkout;

class Donation extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Flashy\Integration\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * Donation constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \AYKO\Donations\Helper\Data $helper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \AYKO\Donations\Helper\Data $helper,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    /**
     * Execute donation action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $response = [];
        try {
            $active = $this->helper->isModuleEnabled();
            if ($active) {
                $quote = $this->checkoutSession->getQuote();
                $amount = $quote->getDonationAmount();
                $post = $this->getRequest()->getPostValue();
                if (isset($post['amount'])) {
                    $newAmount = $this->getRequest()->getParam('amount', 0);
                    if ($this->helper->isDonationAllowed($newAmount)){
                        $baseToQuoteRate = ($quote->getBaseToQuoteRate())?$quote->getBaseToQuoteRate():1;
                        $quote->setDonationAmount($newAmount * $baseToQuoteRate)
                            ->setBaseDonationAmount($newAmount)
                            ->save();
                        $amount = $newAmount;
                    }
                }
                $donationAmounts = $this->helper->getDonationAmounts();
                $donations = [];
                foreach ($donationAmounts as $donationAmount) {
                    $donationAmount['enable'] = $donationAmount['amount'] != $amount;
                    $donations[] = (object)$donationAmount;
                }
                $donations[] = (object)['label' => __('No Donation'), 'amount' => 0, 'enable' => $amount != 0];

                $response = [
                    'success' => true,
                    'active' => $active,
                    'donations' => $donations,
                    'amount' => $amount,
                ];
            } else {
                $response = [
                    'success' => true,
                    'active' => $active,
                    'donations' => [],
                    'amount' => 0,
                ];
            }

        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->helper->logInfo($message);
            $response = [
                'success' => false,
                'value' => __($message)
            ];
        }
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }

}