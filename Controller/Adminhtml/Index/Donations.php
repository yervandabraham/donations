<?php
namespace AYKO\Donations\Controller\Adminhtml\Index;

class Donations extends \Magento\Customer\Controller\Adminhtml\Index
{
    /**
     * Customer donations grid
     *
     * @return \Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
