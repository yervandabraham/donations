<?php
namespace AYKO\Donations\Controller\Adminhtml\Report\Sales;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportDonationsExcel extends \Magento\Reports\Controller\Adminhtml\Report\Sales
{
    /**
     * Export donations report grid to Excel XML format
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function execute()
    {
        $fileName = 'donations.xml';
        $grid = $this->_view->getLayout()->createBlock(\AYKO\Donations\Block\Adminhtml\Sales\Donations\Grid::class);
        $this->_initReportAction($grid);
        return $this->_fileFactory->create($fileName, $grid->getExcelFile($fileName), DirectoryList::VAR_DIR);
    }

    /**
     * Determine if action is allowed for donations report
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('AYKO_Donations::salesroot_donations');
    }
}
