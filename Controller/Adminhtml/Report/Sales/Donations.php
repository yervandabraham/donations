<?php
namespace AYKO\Donations\Controller\Adminhtml\Report\Sales;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use AYKO\Donations\Model\Flag;

class Donations extends \Magento\Reports\Controller\Adminhtml\Report\Sales implements HttpGetActionInterface
{
    /**
     * Donations report action
     *
     * @return void
     */
    public function execute()
    {
        $this->_showLastExecutionTime(Flag::REPORT_DONATION_FLAG_CODE, 'donations');

        $this->_initAction()->_setActiveMenu(
            'AYKO_Donations::report_salesroot_donations'
        )->_addBreadcrumb(
            __('Donations Report'),
            __('Donations Report')
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Donations Report'));

        $gridBlock = $this->_view->getLayout()->getBlock('adminhtml_sales_donations.grid');
        $filterFormBlock = $this->_view->getLayout()->getBlock('grid.filter.form');

        $this->_initReportAction([$gridBlock, $filterFormBlock]);

        $this->_view->renderLayout();
    }

    /**
     * Determine if action is allowed for donations report
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('AYKO_Donations::salesroot_donations');
    }
}
