<?php
namespace AYKO\Donations\Block\Sales\Order;

class Totals extends \Magento\Framework\View\Element\Template
{
    /**
     * Initialize donation totals
     *
     * @return $this
     */
    public function initTotals()
    {
        $donation = $this->getParentBlock()->getSource()->getDonationAmount();
        if($donation == 0) {
            return $this;
        }
        $total = new \Magento\Framework\DataObject(
            [
                'code' => 'donation',
                'value' => $donation,
                'label' => __('Donation'),
            ]
        );

        $this->getParentBlock()->addTotalBefore($total, 'grand_total');
        return $this;
    }
}
