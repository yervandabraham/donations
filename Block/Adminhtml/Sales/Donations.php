<?php
namespace AYKO\Donations\Block\Adminhtml\Sales;

/**
 * Adminhtml donations report page content block
 *
 * @api
 * @since 100.0.2
 */
class Donations extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Template file
     *
     * @var string
     */
    protected $_template = 'Magento_Reports::report/grid/container.phtml';

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_blockGroup = 'AYKO_Donations';
        $this->_controller = 'adminhtml_sales_donations';
        $this->_headerText = __('Total Donations Report');
        parent::_construct();

        $this->buttonList->remove('add');
        $this->addButton(
            'filter_form_submit',
            ['label' => __('Show Report'), 'onclick' => 'filterFormSubmit()', 'class' => 'primary']
        );
    }

    /**
     * Get filter URL
     *
     * @return string
     */
    public function getFilterUrl()
    {
        $this->getRequest()->setParam('filter', null);
        return $this->getUrl('*/*/donations', ['_current' => true]);
    }
}
