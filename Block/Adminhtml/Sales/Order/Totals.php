<?php
namespace AYKO\Donations\Block\Adminhtml\Sales\Order;

class Totals extends \Magento\Framework\View\Element\Template
{
    /**
     * Initialize donation totals
     *
     * @return $this
     */
    public function initTotals()
    {
        $source = $this->getParentBlock()->getSource();
        $donation = $source->getDonationAmount();
        if($donation == 0) {
            return $this;
        }
        $total = new \Magento\Framework\DataObject(
            [
                'code' => 'donation',
                'value' => $donation,
                'base_value' => $source->getBaseDonationAmount(),
                'label' => __('Donation'),
            ]
        );

        $this->getParentBlock()->addTotalBefore($total, 'grand_total');
        return $this;
    }
}
