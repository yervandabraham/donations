<?php
namespace AYKO\Donations\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Class DonationAmounts
 * @package AYKO\Donations\Block\Adminhtml\Form\Field
 */
class DonationAmounts extends AbstractFieldArray
{
    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender()
    {
        $this->addColumn('label', ['label' => __('Donation label'), 'class' => 'required-entry']);
        $this->addColumn('amount', ['label' => __('Donation amount'), 'class' => 'required-entry']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Donation Amount');
    }
}
