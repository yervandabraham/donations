<?php
namespace AYKO\Donations\Block\Adminhtml\Edit\Tab;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Ui\Component\Layout\Tabs\TabInterface;

class Donation extends \Magento\Framework\View\Element\Template implements TabInterface
{
    /**
     * @var string
     */
    protected $_template = 'AYKO_Donations::tab/donation.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollectionFactory;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    protected $donations;

    /**
     * Donation constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_priceCurrency = $priceCurrency;
        parent::__construct($context, $data);
    }

    /**
     * Return Tab label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Donations');
    }

    /**
     * Return Tab title
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Donations');
    }

    /**
     * Tab class getter
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return '';
    }

    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return false;
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Get customer donations
     *
     * @return bool|\Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getDonations()
    {
        if (!$this->donations) {
            $customerId = $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
            $this->donations = $this->_orderCollectionFactory->create($customerId)->addFieldToSelect(
                'base_donation_amount'
            )->addFieldToFilter(
                'base_donation_amount',
                ['neq' => 0]
            );
        }
        return $this->donations;
    }

    /**
     * Get customer donations sum
     *
     * @return float
     */
    public function getDonationsTotal()
    {
        $donationsTotal = 0.00;
        foreach ($this->getDonations() as $donation){
            $donationsTotal += $donation->getBaseDonationAmount();
        }
        return $donationsTotal;
    }

    /**
     * Get formatted price
     *
     * @param float
     *
     * @return string
     */
    public function getFormattedPrice($amount)
    {
        return $this->_priceCurrency->convertAndFormat($amount);
    }
}
