<?php
namespace AYKO\Donations\Block\Donation;

/**
 * Donation history block
 *
 * @api
 * @since 100.0.2
 */
class History extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'AYKO_Donations::donation/history.phtml';

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollectionFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Sales\Model\Order\Config
     */
    protected $_orderConfig;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    protected $donations;

    /**
     * @var float
     */
    protected $totalDonation;

    /**
     * @var float
     */
    protected $actualTotalDonation;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;

    /**
     * History constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_customerSession = $customerSession;
        $this->_orderConfig = $orderConfig;
        $this->_priceCurrency = $priceCurrency;
        parent::__construct($context, $data);
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Donations'));
    }

    /**
     * Get customer donations
     *
     * @return bool|\Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getDonations()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        if (!$this->donations) {
            $this->donations = $this->_orderCollectionFactory->create($customerId)->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'status',
                ['in' => $this->_orderConfig->getVisibleOnFrontStatuses()]
            )->addFieldToFilter(
                'donation_amount',
                ['neq' => 0]
            )->setOrder(
                'created_at',
                'desc'
            );
        }
        return $this->donations;
    }

    /**
     * Get customer donations sum
     *
     * @return bool|float
     */
    public function getDonationsTotal()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }

        if (!$this->totalDonation) {
            $collection = $this->_orderCollectionFactory->create($customerId)
                ->addFieldToSelect('base_donation_amount')
                ->addFieldToSelect('base_donation_canceled')
                ->addFieldToFilter('status',['in' => $this->_orderConfig->getVisibleOnFrontStatuses()])
                ->addFieldToFilter('base_donation_amount', ['neq' => 0]);
            $collection->getSelect()
                ->columns(['total_donation' => new \Zend_Db_Expr('SUM(IFNULL(base_donation_amount,0) - IFNULL(base_donation_canceled,0))')])
                ->group('customer_id');
            if($collection->getSize()){
                $this->totalDonation = $collection->getFirstItem()->getTotalDonation();
            } else {
                $this->totalDonation = 0.00;
            }
        }

        return $this->totalDonation;
    }

    /**
     * Get customer actual donations sum
     *
     * @return bool|float
     */
    public function getActualDonationsTotal()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }

        if (!$this->actualTotalDonation) {
            $collection = $this->_orderCollectionFactory->create($customerId)
                ->addFieldToSelect('base_donation_invoiced')
                ->addFieldToSelect('base_donation_refunded')
                ->addFieldToFilter( 'status', ['in' => $this->_orderConfig->getVisibleOnFrontStatuses()])
                ->addFieldToFilter('base_donation_invoiced',['neq' => 0]);
            $collection->getSelect()
                ->columns(['actual_total_donation' => new \Zend_Db_Expr('SUM(IFNULL(base_donation_invoiced,0) - IFNULL(base_donation_refunded,0))')])
                ->group('customer_id');
            if($collection->getSize()){
                $this->actualTotalDonation = $collection->getFirstItem()->getActualTotalDonation();
            } else {
                $this->actualTotalDonation = 0.00;
            }
        }

        return $this->actualTotalDonation;
    }

    /**
     * Get formatted price
     *
     * @param float
     *
     * @return string
     */
    public function getFormattedPrice($amount)
    {
        return $this->_priceCurrency->convertAndFormat($amount);
    }

    /**
     * @inheritDoc
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getDonations()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'donation.history.pager'
            )->setCollection(
                $this->getDonations()
            );
            $this->setChild('pager', $pager);
            $this->getDonations()->load();
        }
        return $this;
    }

    /**
     * Get Pager child block output
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get order view URL
     *
     * @param object $order
     * @return string
     */
    public function getViewUrl($order)
    {
        return $this->getUrl('sales/order/view', ['order_id' => $order->getId()]);
    }
}
